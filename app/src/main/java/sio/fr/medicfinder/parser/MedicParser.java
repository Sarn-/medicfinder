package sio.fr.medicfinder.parser;

/**
 * Created by Horoneru on 01/12/2015.
 */

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

import sio.fr.medicfinder.Gps;
import sio.fr.medicfinder.entity.Medic;

public class MedicParser {

    public ArrayList<Medic> pharmacies;

    private final JSONObject json;
    private final String KEY_ARRAY = "pharmacies";
    private final String KEY_NOM = "rs";
    private final String KEY_VILLE = "commune";
    private final String KEY_CP = "cp";
    private final String KEY_LONGITUDE = "lng";
    private final String KEY_LATITUDE = "lat";
    private final String KEY_PHONE = "telephone";
    private final String KEY_FAX = "telecopie";
    private final String KEY_DATEOUV = "dateouv";
    private final String KEY_RSCOMPLETE = "rslongue";

    public MedicParser(JSONObject json){
        this.json = json;

        this.pharmacies = new ArrayList<>();
    }

    public void parseNow(){

        try {
            JSONArray liste = json.getJSONArray(KEY_ARRAY);
            for(int i=0;i< liste.length();i++){

                JSONObject jo = liste.getJSONObject(i);

                LatLng mapPoint = new LatLng(jo.getDouble(KEY_LATITUDE), jo.getDouble(KEY_LONGITUDE));
                Medic pharmarcie = new Medic(jo.getString(KEY_NOM),jo.getString(KEY_CP),
                        mapPoint, jo.getString(KEY_VILLE), jo.getString(KEY_DATEOUV));

                getString(jo, pharmarcie, KEY_PHONE);
                getString(jo, pharmarcie, KEY_FAX);
                getString(jo, pharmarcie, KEY_RSCOMPLETE);
                pharmarcie.setDistance(distancePharmacie(mapPoint));

                this.pharmacies.add(pharmarcie);
            }

        } catch (JSONException e) {

            e.printStackTrace();

        }
    }

    private void getString(JSONObject jo, Medic pharmarcie, String key) throws JSONException {
        if(!jo.isNull(key))
        {
            switch (key)
            {
                case KEY_PHONE:
                    pharmarcie.setTelephone("0" + jo.getString(KEY_PHONE));
                    break;
                case KEY_FAX:
                    pharmarcie.setTelecopie(jo.getString(KEY_FAX));
                    break;
                case KEY_RSCOMPLETE:
                    pharmarcie.setRaisonSocialeComplete(
                            jo.getString(KEY_RSCOMPLETE));
                    break;
            }
        }
    }

    /**
     * distancePharmacie est appelée par la méthode filterPharmacies. Elle calcule la distance en km entre un point A et un point B
     * Elle est utilisée pour calculer la distance entre l'utilisateur et une pharmarcie
     * @param pharmarciesPosition entité représentant la position d'une pharmarcie
     * @return la distance entre les deux éléments en km
     */
    private double distancePharmacie(LatLng pharmarciesPosition) {
        //Init variables
        final double RAYONTERRE = 6372.795477598; //En km

        final double LATITUDEGPS = Math.toRadians(Gps.getInstance().getLatitude());
        final double LONGITUDEGPS = Math.toRadians(Gps.getInstance().getLongitude());

        final double LATITUDEPHARMARCIE = Math.toRadians(pharmarciesPosition.latitude);
        final double LONGITUDEPHARMARCIE = Math.toRadians(pharmarciesPosition.longitude);

        //Calcul & return
        final double resultat =
                RAYONTERRE * Math.acos((Math.sin(LATITUDEGPS) * Math.sin(LATITUDEPHARMARCIE) +
                        Math.cos(LATITUDEGPS) * Math.cos(LATITUDEPHARMARCIE) *
                                Math.cos(LONGITUDEGPS - LONGITUDEPHARMARCIE)));
        return Math.round(resultat * 100.0) / 100.0;
    }
}
