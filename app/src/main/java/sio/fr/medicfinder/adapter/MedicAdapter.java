package sio.fr.medicfinder.adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;

import sio.fr.medicfinder.Gps;
import sio.fr.medicfinder.MainActivity;
import sio.fr.medicfinder.R;
import sio.fr.medicfinder.entity.Medic;

/**
 * Created by Horoneru on 01/12/2015.
 */

public class MedicAdapter extends ArrayAdapter<Medic> {

    private MainActivity mainActivity;


    public MedicAdapter(MainActivity context, ArrayList<Medic> Pharmacies) {
        super(context, R.layout.activity_main, Pharmacies);
        this.mainActivity = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = mainActivity.getLayoutInflater();

        View listViewMedic = inflater.inflate(R.layout.element_liste, null, true);

        TextView nomPharmacie = (TextView) listViewMedic.findViewById(R.id.nomPharmacie);

        TextView distanceTextView = (TextView) listViewMedic.findViewById(R.id.distance);

        TextView communeCp = (TextView) listViewMedic.findViewById(R.id.communeCp);

        nomPharmacie.setText(getItem(position).getNomPharmacie());

        distanceTextView.setText(String.format("à %skm", getItem(position).getDistance()));

        communeCp.setText(String.format("%s, %s",
                getItem(position).getCommune(),
                getItem(position).getCommuneCp()));

        Animation fadeIn = null;
        fadeIn = AnimationUtils.loadAnimation(mainActivity, R.anim.abc_fade_in);
        fadeIn.setDuration(200);
        listViewMedic.startAnimation(fadeIn);
        return listViewMedic;
    }
    }