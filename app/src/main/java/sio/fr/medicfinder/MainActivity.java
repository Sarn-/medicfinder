package sio.fr.medicfinder;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import sio.fr.medicfinder.adapter.MedicAdapter;
import sio.fr.medicfinder.entity.Medic;
import sio.fr.medicfinder.parser.MedicParser;

/**
 * MainActivity permettra d'afficher la liste de medics, et pourra filtrer selon leurs coordonnées
 * relative à la position de l'utilisateur.
 * Elle abritera la requête volley (Maxime), et le setup de la liste et de l'adapter (Julien).
 * Il faudra aussi permettre de mettre à jour l'adapter lors d'un filtre <- Julien - Quentin
 */
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,
        TextView.OnEditorActionListener {

    //Attributes
    private EditText filtreEditText;
    private ListView pharmarciesListView;
    private ArrayList<Medic> pharmacies;
    private final int TIMEOUT_TIME_JSON = 6000;

    //Activity Life cycles
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pharmarciesListView = (ListView) findViewById(R.id.listePharmacies);
        filtreEditText = (EditText) findViewById(R.id.filtreEditText);
        filtreEditText.setOnEditorActionListener(this);
        downloadJson();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.exit(0); //WORKAROUND. Sinon l'appli ne quitte pas réellement.
    }

    //Methods

    /**
     * downloadJson est la méthode où la requête Volley sera faite.
     * Une fois le contenu reçu, la méthode enverra la réponse Json à la méthode parseJson()
     * parseJson() enverra ensuite à showJson(), qui mettra en place l'adapter et montrera la liste
     */
    private void downloadJson() {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(
                Request.Method.GET, "http://109.15.49.137/public_html/Medicfinder/api/json.php",
                "", new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                parseJson(response);
            }

        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
            }
        });

        jsonRequest.setRetryPolicy(
                new DefaultRetryPolicy(TIMEOUT_TIME_JSON,
                        1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)); //Permet d'allonger le timeout de base
        //Ainsi que de retry une fois si besoin
        //ajoute dans la liste d'attente la requete
        queue.add(jsonRequest);
    }

    /** parseJson parse le json via une instance de MedicParser
     * @param json le json que l'on souhaite parser
     */
    private void parseJson(JSONObject json) {
        MedicParser medicParser = new MedicParser(json);
        medicParser.parseNow();
        pharmacies = medicParser.pharmacies;
        showJson(pharmacies);
    }

    /** showJson affiche le json
     * Elle adapte et affiche la liste de pharmarcies
     * @param pharmacies une ArrayList de medic représentant les pharmarcies, fournies par parseJson
     */
    private void showJson(ArrayList<Medic> pharmacies) {
        Collections.sort(pharmacies, Medic.MEDIC_DISTANCE_COMPARATOR);
        MedicAdapter medicAdapter = new MedicAdapter(this, pharmacies);
        pharmarciesListView.setAdapter(medicAdapter);
        pharmarciesListView.setOnItemClickListener(this);

        //Active le bouton lorsque les items sont affichés, et enlève le loading
        setListReady();
    }

    private void setListReady() {
        ProgressBar loading = (ProgressBar) findViewById(R.id.loading);
        Button showAllOnMapsButton = (Button) findViewById(R.id.showAllOnMapsButton);
        showAllOnMapsButton.setEnabled(true);
        loading.setVisibility(View.GONE);
    }

    /**
     * filterPharmacies est appelé lors d'une validation du champ de filtrage
     * Elle filtre par km. L'utilisateur marquera un nombre de kilomètres afin d'avoir
     * les pharmarcies se situant dans ce perimètre.
     */
    public void filterPharmacies() {
        final int KMDISTANCE =
                Integer.parseInt(filtreEditText.getText().toString());
        ArrayList<Medic> pharmaciesTriees = new ArrayList<>();

        for (Medic pharmacie : pharmacies) {
            if(pharmacie.getDistance() <= KMDISTANCE)
                pharmaciesTriees.add(pharmacie);
        }

        pharmarciesListView.setAdapter(
                new MedicAdapter(this, pharmaciesTriees)); //Plus simple à gérer :V
    }

    /**
     * showAllMaps est appelé par le bouton showAllOnMaps.
     * Elle appele la vue MapsActivity et affichera chacun des éléments de la liste sur maps
     */
    public void showAllOnMaps(View v) {

        final int itemCount = pharmarciesListView.getCount();
        String[] nomPharmacies = new String[itemCount];
        double[] latitudePharmacies = new double[itemCount];
        double[] longitudePharmacies = new double[itemCount];
        final MedicAdapter medicAdapter = (MedicAdapter) pharmarciesListView.getAdapter();

        for(int i = 0; i < itemCount; i++ )
        {
            nomPharmacies[i] = medicAdapter.getItem(i).getNomPharmacie();
            latitudePharmacies[i] = medicAdapter.getItem(i).getMapPoint().latitude;
            longitudePharmacies[i] = medicAdapter.getItem(i).getMapPoint().longitude;
        }

        Intent mapsIntent = new Intent(this, MapsActivity.class);
        mapsIntent.putExtra("nombrePharmacies", itemCount);
        mapsIntent.putExtra("nomPharmacies", nomPharmacies);
        mapsIntent.putExtra("latitudePharmacies", latitudePharmacies);
        mapsIntent.putExtra("longitudePharmacies", longitudePharmacies);

        startActivity(mapsIntent);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.info_pharmacie);
        dialog.setTitle("Détails");

        TextView nomPharmacie = (TextView) dialog.findViewById(R.id.nomPharmTextView);
        TextView creationPharmacie = (TextView) dialog.findViewById(R.id.creationPharmTextView);
        TextView villeEtCp = (TextView) dialog.findViewById(R.id.adressPharmTextView);
        TextView telephone = (TextView) dialog.findViewById(R.id.telephonePharmTextView);
        TextView distance = (TextView) dialog.findViewById(R.id.distancePharmTextView);

        final Medic selectedMedic = (Medic) adapterView.getItemAtPosition(i);

        String nomPharmacieValue;

        // Il se peut que la raison sociale soit vide
        // à ce moment là, on préfère prendre le nom "normal"
        if(selectedMedic.getRaisonSocialeComplete() == null)
            nomPharmacieValue = selectedMedic.getNomPharmacie();
        else
            nomPharmacieValue = selectedMedic.getRaisonSocialeComplete();
        nomPharmacie.setText(nomPharmacieValue);

        creationPharmacie.setText(String.format("Ouverte depuis le %s",
                selectedMedic.getDateOuverture()));

        villeEtCp.setText(String.format("%s, %s",
                selectedMedic.getCommune(), selectedMedic.getCommuneCp()));

        if(selectedMedic.getTelephone() != null)
            telephone.setText(String.format("Téléphone : %s",
                    selectedMedic.getTelephone()));

        distance.setText(String.format("à %skm",
                selectedMedic.getDistance()));

        Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button mapButton = (Button) dialog.findViewById(R.id.showMapsButton);
        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapsIntent = new Intent(MainActivity.this, MapsActivity.class);
                mapsIntent.putExtra("nombrePharmacies", 1);

                String[] nomPharmacie = new String[1];
                double[] latitudePharmacie = new double[1];
                double[] longitudePharmacie = new double[1];

                nomPharmacie[0] = selectedMedic.getNomPharmacie();
                latitudePharmacie[0] = selectedMedic.getMapPoint().latitude;
                longitudePharmacie[0] = selectedMedic.getMapPoint().longitude;


                mapsIntent.putExtra("nomPharmacies", nomPharmacie);
                mapsIntent.putExtra("latitudePharmacies", latitudePharmacie);
                mapsIntent.putExtra("longitudePharmacies", longitudePharmacie);

                startActivity(mapsIntent);
            }
        });
        dialog.show();
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (keyEvent == null) {
            if (actionId == EditorInfo.IME_ACTION_DONE ||
                    actionId == EditorInfo.IME_ACTION_NEXT) // <- ACTION_DONE est remplacé par
                                                            //    ACTION_NEXT lorsque que l'on passe
                                                            //    à la vue map (BUG ?)
                if(textView.getText().length() == 0)
                    return false;
                else
                    filterPharmacies();
            else
                return false;  // Le système s'occupe du reste
        }
        return true;
    }
}
