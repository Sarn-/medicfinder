package sio.fr.medicfinder.entity;


/**
 * Created by Horoneru on 01/12/2015.
 */

import com.google.android.gms.maps.model.LatLng;
import java.util.Comparator;

/**

 * La classe Medic est une entité permettant de représenter une pharmacie
 * Elle sera utilisée par le MedicParser pour créer des Medic.

 */

public class Medic {

    public static final Comparator<Medic> MEDIC_DISTANCE_COMPARATOR = new Comparator<Medic>() {
        @Override
        public int compare(Medic medic, Medic t1) {
            return Double.compare(medic.getDistance(), t1.getDistance());
        }
    };

    private String nomPharmacie;
    private String communeCp;
    private double distance;
    private LatLng mapPoint;
    private String commune;
    private String telephone;
    private String telecopie;
    private String dateOuverture;
    private String raisonSocialeComplete;

    public Medic(String nomPharmacie, String communeCp, LatLng mapPoint,
                 String commune, String dateOuverture) {
        this.nomPharmacie = nomPharmacie;
        this.communeCp = communeCp;
        this.mapPoint = mapPoint;
        this.commune = commune;
        this.dateOuverture = dateOuverture;
    }

    public void setNomPharmacie(String nomPharmacie) {
        this.nomPharmacie = nomPharmacie;
    }

    public void setCommuneCp(String communeCp) {
        this.communeCp = communeCp;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public LatLng getMapPoint() {
        return mapPoint;
    }

    public void setMapPoint(LatLng mapPoint) {
        this.mapPoint = mapPoint;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelecopie() {
        return telecopie;
    }

    public void setTelecopie(String telecopie) {
        this.telecopie = telecopie;
    }

    public String getDateOuverture() {
        return dateOuverture;
    }

    public void setDateOuverture(String dateOuverture) {
        this.dateOuverture = dateOuverture;
    }


    public String getRaisonSocialeComplete() {
        return raisonSocialeComplete;
    }

    public void setRaisonSocialeComplete(String raisonSocialeComplete) {
        this.raisonSocialeComplete = raisonSocialeComplete;
    }

    public String getNomPharmacie() {
        return nomPharmacie;
    }

    public String getCommuneCp() {
        return communeCp;
    }

    public double getDistance() {
        return distance;
    }
}
