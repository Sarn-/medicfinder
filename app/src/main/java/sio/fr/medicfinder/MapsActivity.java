package sio.fr.medicfinder;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import sio.fr.medicfinder.tracker.GpsTracker;

public class    MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap googleMap;
    private String[] nomPharmacies;
    private double[] latitudePharmacies;
    private double[] longitudePharmacies;
    private int nombrePharmarcies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        retrieveExtras();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void retrieveExtras() {
        Bundle extras = getIntent().getExtras();
        nombrePharmarcies = extras.getInt("nombrePharmacies");
        nomPharmacies = extras.getStringArray("nomPharmacies");
        latitudePharmacies = extras.getDoubleArray("latitudePharmacies");
        longitudePharmacies = extras.getDoubleArray("longitudePharmacies");
    }


    // Méthode appelée si l'utilisateur possède Google maps.
    // Elle init google Maps en positionnant la caméra et les markers
    @Override
    public void onMapReady(GoogleMap googleMap) {
        final GpsTracker gps = Gps.getInstance();
        final LatLng GPSPOSITION = new LatLng(gps.getLatitude(), gps.getLongitude());
        this.googleMap = googleMap;

        for (int i = 0; i < nombrePharmarcies; i++)
        {
            double latitudePharmacie = latitudePharmacies[i];
            double longitudePharmacie = longitudePharmacies[i];
            String nomPharmacie = nomPharmacies[i];
            LatLng pharmacie = new LatLng(latitudePharmacie, longitudePharmacie);
            googleMap.addMarker(new MarkerOptions()
                    .position(pharmacie)
                    .title(nomPharmacie));
            if(nombrePharmarcies == 1)
            {
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(pharmacie));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pharmacie, 13));
            }
            else
            {
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(GPSPOSITION));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(GPSPOSITION, 12));
            }
        }
        googleMap.addMarker(new MarkerOptions()
                .position(GPSPOSITION)
                .title("Votre position")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

    }
}