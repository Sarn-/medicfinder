package sio.fr.medicfinder;

import sio.fr.medicfinder.tracker.GpsTracker;

/**
 * Created by Julien on 23/12/2015.
 */
public class Gps {
    private static GpsTracker ourInstance = new GpsTracker();

    public static GpsTracker getInstance() {
        return ourInstance;
    }

    private Gps() {
    }
}
